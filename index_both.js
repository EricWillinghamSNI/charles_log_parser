var JSONPath = require('advanced-json-path');
var fs = require('fs');
var csv = require('fast-csv');
var math = require('mathjs');
var URL = require('url-parse');
var fileExtension = require('file-extension');

var path = 'Logs/Unprocessed';
var targetPath = 'Logs/Processed';
//var filename = 'doublemainstart_710';
var analyticsURLarray = [
    "sa.hgtv.com", 
    "ssa.food.com",
    "sa.foodnetwork.com",
    "ssa.foodnetwork.com",
    "sa.geniuskitchen.com",
    "sa.diynetwork.com",
    "metrics.travelchannel.com",
    "sa.cookingchanneltv.com"
];
var heartbeatURLarray = ["ewscripps.hb.omtrdc.net"];

const type = {
    Heartbeat: 'Heartbeat',
    Analytics: 'Analytics',
    Other: 'Other'
}

///Users/163420/Projects/snippet/Logs/Unprocessed/doublemainstart_710.json
///Users/163420/Projects/snippet/Logs/Unprocessed/event24_issue.json
///Users/163420/Projects/snippet/ModuleClickTracking/clicktracking_11.json



// fs.readFile('file.json', 'utf8', function (err, data) {
//   if (err) throw err;
//   //console.log(data);
//   object = JSON.parse(data);
// });
//var object = JSON.parse(fs.readFileSync('file', 'utf8'));


function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

function parseKeyValue(name, value, Type, callObject, analyticsheaderObject, hb_headerObject) {
    if (value != undefined) {
        if (decodeURIComponent(String(value)).length >= 13) {
            callObject[decodeURIComponent(name)] = '="' + decodeURIComponent(value) + '"';
        } else {
            callObject[decodeURIComponent(name)] = decodeURIComponent(value);
        }
    }
    if (value != "") {
        if (Type == type.Analytics) {
            analyticsheaderObject[name] = '';

        } else if (Type == type.Heartbeat) {
            hb_headerObject[name] = '';

        }
        // headerObject[name] = '';
    }

}

function valueIndexOfArray(value, array) {

    //console.log(" value ->"+value);
    for (var i = 0; i < array.length; i++) {
        if (value.indexOf(array[i]) >= 0) {
            return true;
        }
    }
    return false;

}

function writePostToArray(tCallObject, analyticsCallArray, hbCallArray) {
    if (!isEmpty(tCallObject)) {


        if (valueIndexOfArray(tCallObject['Host'], analyticsURLarray)) {
            analyticsCallArray.push(tCallObject);
        } else if (valueIndexOfArray(tCallObject['Host'], heartbeatURLarray)) {
            hbCallArray.push(tCallObject);
        }
    }
}

function processJSONFile(object, filename, path, targetPath) {
    var data = JSONPath(object, "$.log.entries");
    var callObject = {
        'parsingOrder':'',        
        'type': ''
    };
    var analyticsCallArray = [];
    var hbCallArray = [];

    var analyticsheaderObject = {
        'parsingOrder':'',
        'type': '',
        'startDateTime':''
    };
    var hb_headerObject = {
        'parsingOrder':'',        
        'type': '',
        'startDateTime':''
    };




    for (var i = 0; i < data.length; i++) {
        //  console.log('=====================');
        headers = JSONPath(data[i], "$.request.headers");
        fullURL = JSONPath(data[i], "$.request.url");
        callObject = {};

        Type = type.Other;
        var url = URL(fullURL);
        callObject.startDateTime = data[i].startedDateTime;

        callObject['Host'] = url.host;
        callObject['parsingOrder'] = i;

        analyticsheaderObject['Host'] = '';
        analyticsheaderObject['startDateTime'] = '';
        
        hb_headerObject['Host'] = '';
        hb_headerObject['startDateTime'] = '';

        isAnalytics = valueIndexOfArray(url.host, analyticsURLarray);
        isHeartbeat = valueIndexOfArray(url.host, heartbeatURLarray);

        if (!isAnalytics && !isHeartbeat) {
            // console.log('skipping row for ' + url.host);
            continue;
        } else if (isAnalytics) {
            Type = type.Analytics;
        } else if (isHeartbeat) {
            Type = type.Heartbeat;
        }

        contentType = JSONPath(data[i], "$.request.headers[?(@.name == 'Content-Type')].value");
        method = JSONPath(data[i], "$.request.method");
        // console.log(data[i].startedDateTime + "  " + url.host + "   " + method + "   " + contentType);

        params = "";

        if (data[i].request.hasOwnProperty('queryString') && data[i].request.queryString.length > 0) {
            queryString = data[i].request.queryString;
            for (var j = 0; j < queryString.length; j++) {
                parseKeyValue(queryString[j].name, queryString[j].value, Type, callObject, analyticsheaderObject, hb_headerObject);
            }
            writePostToArray(callObject, analyticsCallArray, hbCallArray);
            callObject['type'] = "queryString";

        } else if (data[i].request.hasOwnProperty('postData')) {
            if (data[i].request.postData.hasOwnProperty('params')) {
                params = data[i].request.postData.params;
                for (var s = 0; s < params.length; s++) {
                    parseKeyValue(params[s].name, params[s].value, Type, callObject, analyticsheaderObject, hb_headerObject);
                }
                writePostToArray(callObject, analyticsCallArray, hbCallArray);
                callObject['type'] = "postData_params";


            } else if (data[i].request.postData.hasOwnProperty('text')) {
                params = data[i].request.postData.text.split('&');
                if (params !== undefined) {
                    for (var s = 0; s < params.length; s++) {
                        param = params[s].split('=');
                        parseKeyValue(param[0], param[1], Type, callObject, analyticsheaderObject, hb_headerObject);
                    }
                    writePostToArray(callObject, analyticsCallArray, hbCallArray);
                    callObject['type'] = "postData_text";


                }
            }
        }
        if (data[i].request.hasOwnProperty('headers')) {
            headers = data[i].request.headers;

            for (var k = 0; k < headers.length; k++) {
                var name = headers[k].name;
                var value = headers[k].value;
                if (name == "Host")
                    if (valueIndexOfArray(value, analyticsURLarray)) {
                        callObject[name] = value;
                        analyticsheaderObject[name] = '';
                    } else if (valueIndexOfArray(value, heartbeatURLarray)) {
                    callObject[name] = value;
                    hb_headerObject[name] = '';
                }
            }
        }

    }


    analyticsCallArray.unshift(analyticsheaderObject);
    hbCallArray.unshift(hb_headerObject);

    //console.log('./' + targetPath + '/' + filename + '_sa.csv');
    var ws = fs.createWriteStream('./' + targetPath + '/' + filename + '_sa.csv');
    csv
        .write(analyticsCallArray, {
            headers: true
        })
        .pipe(ws);

    var ws = fs.createWriteStream('./' + targetPath + '/' + filename + '_hb.csv');
    csv
        .write(hbCallArray, {
            headers: true
        })
        .pipe(ws);
}
//var path = 'MOM-4273';


fs.readdir(path, function (err, items) {
    console.log("Path==>" + path + " items-" + items.length);



    for (var k = 0; k < items.length; k++) {

        var targetPath = 'Logs/Processed';

        if (fileExtension(items[k]) == 'json') {

            var object = require('./' + path + '/' + items[k]);
            var targetDirectory = items[k].split(".")[0];
            var filename = items[k];

            console.log('"Checking ./' + path + '/' + filename);
            try {
                fs.statSync('./' + targetPath + '/' + targetDirectory);
            } catch (e) {
                fs.mkdirSync('./' + targetPath + '/' + targetDirectory);
            }

            console.log('Parsing logs for ... ' + path + '/' + items[k]);
            console.log('---');
            
            processJSONFile(object, targetDirectory, path, targetPath + "/" + targetDirectory);

            console.log("Moving "+filename +" to Processed folder" );
            fs.rename('./' + path + '/' + filename, targetPath + "/" + targetDirectory + '/' + filename, function (err) {
                if (err) {
                    console.log("Error: " + err);
                    throw err
                }
            });
            console.log("Moving "+filename.slice(0,-4)+".chls to Processed folder" );
            fs.rename('./' + path + '/' + filename.slice(0,-5)+".chls", targetPath + "/" + targetDirectory + '/' + filename.slice(0,-5)+".chls", function (err) {
                    if (err) {
                        console.log("Error: " + err);
                        throw err
                    }
                });
        }
    }
});