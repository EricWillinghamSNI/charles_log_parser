var JSONPath = require('advanced-json-path');
var fs = require('fs');
var csv = require('fast-csv');
var URL = require('url-parse');

var path = 'Unprocessed';
var filename = 'event24_issue';
var HeartbeatURL = "ewscripps.hb.omtrdc.net";

var object = require('./' + path + '/' + filename);
console.log('Parsing logs for ... ' + path + '/' + filename);


//Users/163420/Projects/snippet/VP-4862/VP-4862.chls
// fs.readFile('file.json', 'utf8', function (err, data) {
//   if (err) throw err;
//   //console.log(data);
//   object = JSON.parse(data);
// });
//var object = JSON.parse(fs.readFileSync('file', 'utf8'));



var data = JSONPath(object, "$.log.entries");
var callObject = {};
var callArray = [];
var HBCallArray = [];
var headerObject = {
    'startDateTime':'',
    'Host':'',
    's:asset:type': '',
    's:event:type': '',
    'l:event:playhead': '',
    's:aam:blob': '',
    'l:aam:loc_hint': '',
    's:user:mid': '',
    'l:event:ts': '',
    'l:event:prev_ts': ''
};;


//headerObject['startDateTime'] = '';
//headerObject['Host'] = '';


for (var i = 0; i < data.length; i++) {
    callObject = {};
    fullURL = JSONPath(data[i], "$.request.url");

    var url = URL(fullURL);
    callObject.startDateTime = data[i].startedDateTime;
    headerObject['startDateTime'] = '';
    callObject['Host'] = url.host;

    params = "";
    if (data[i].request.hasOwnProperty('queryString')) {
        queryString = data[i].request.queryString;
        for (var j = 0; j < queryString.length; j++) {
            /* if(queryString[j].name == 's:user:mid'){
            console.log(queryString[j].value);
        }*/
            if (decodeURIComponent(queryString[j].value).length >= 13) {

                callObject[decodeURIComponent(queryString[j].name)] = '="' + decodeURIComponent(queryString[j].value) + '"';
            } else {
                callObject[decodeURIComponent(queryString[j].name)] = decodeURIComponent(queryString[j].value);
            }
            //callObject[queryString[j].name] = queryString[j].value;
            headerObject[queryString[j].name] = '';
        }

         if (callObject['Host'].indexOf(HeartbeatURL) > -1 ){
                    callArray.push(callObject);
                }
        // console.log(callObject);
       // if (callObject.hasOwnProperty('Host') && callObject['Host'] == HeartbeatURL) {
          //  callArray.push(callObject);
       // }
    }
   
    if (data[i].request.hasOwnProperty('headers')) {
        headers = data[i].request.headers;

        for (var k = 0; k < headers.length; k++) {
            if (headers[k].name == "Host" && headers[k].value === HeartbeatURL) {
                //console.log(headers[k].value);
                 callObject[headers[k].name] = headers[k].value;
                headerObject[headers[k].name] = '';
           // callArray.push(callObject);
                

            }
        }
       /* if (callObject.hasOwnProperty('Host') && callObject['Host'] == HeartbeatURL) {
            callArray.push(callObject);
            console.log('Writting Header');
        }*/
    }

}
//console.log(callArray);


callArray.unshift(headerObject);

var ws = fs.createWriteStream('./' + path + '/' + filename + '_hb.csv');
csv
    .write(callArray, {
        headers: true
    })
    .pipe(ws);