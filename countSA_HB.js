var JSONPath = require('advanced-json-path');
var fs = require('fs');
var csv = require('fast-csv');
var math = require('mathjs');
var URL = require('url-parse');
var bodyParser = require('body-parser')
var path = 'MOM-4273';
var fileExtension = require('file-extension');

var firebase = require('firebase');

firebase.initializeApp({databaseURL: "https://sni-heartbeast-posts.firebaseio.com"});


var jsonClient =
    new FirebaseREST.JSONClient('https://sni-heartbeast-posts.firebaseio.com', {
        auth: '0Dd1ARCQ8P5ZPk3iVUXoTc5UsQwR6RjVVJ5hyK48'
    });



//jsonClient.put('/config/Browser', callBody2).then(console.log("Config Posted"));

var sum_saqsCount = 0;
var sum_sapdCount = 0;
var sum_saCount = 0;
var sum_hbCount = 0;
var sum_hbqsCount = 0;
var sum_hbpdCount = 0;
var sum_otherCount = 0;

var newCount = jsonClient.get('/Count').then(console.log).then(
fs.readdir(path, function (err, items) {
    console.log("Path==>" + path + " items-" + items.length);

    for (var i = 0; i < items.length; i++) {

        var origfile = './' + path + '/' + items[i];
        var origfilepath = './' + path + '/JSONfiles/' + items[i];
        var origfilename = origfile.replace(/ /g, "_").split(".")[1];
        var origfilepathname = origfilepath.replace(/ /g, "_").split(".")[1];

        if (fileExtension(items[i]) == 'json') {

            // console.log("parsing" + origfile);
            var object = require(origfile);

            var data = JSONPath(object, "$.log.entries");

            var hbCount = 0;
            var saCount = 0;
            var otherCount = 0;

            var qsCount = 0;
            var saqsCount = 0;
            var hbqsCount = 0;

            var pdCount = 0;
            var sapdCount = 0;
            var hbpdCount = 0;
            var otherCount = 0;
            var sumCount = {};

            for (var j = 0; j < data.length; j++) {

                fullURL = JSONPath(data[j], "$.request.url");
                querystring = JSONPath(data[j], "$.request.queryString");
                postdata = JSONPath(data[j], "$.request.postData.params");
                startedDateTime = JSONPath(data[j], "$.startedDateTime");

                var track = false;

                sumCount = {};

                url = URL(fullURL);

                if (url.host.indexOf('sa.') >= 0) {
                    var fbObject = {
                        "startedDateTime": startedDateTime,
                        "filename": items[i],
                        "host": url.host
                    };

                    if (querystring.length >= 1) {
                        fbObject.querystring = querystring;
                        saqsCount += 1;
                    }
                    if (postdata.length > 1) {
                        fbObject.postdata = postdata;
                        sapdCount += 1;
                    }
                    saCount += 1;
                    track= true;
                } else if (url.host.indexOf("ewscripps.hb.omtrdc.net") >= 0) {
                    var fbObject = {
                        "startedDateTime": startedDateTime,
                        "host": url.host,
                        "filename": items[i]
                    };

                    if (querystring.length >= 1) {
                        fbObject.querystring = querystring;
                        hbqsCount += 1;
                    }
                    if (postdata.length > 1) {
                        fbObject.postdata = postdata;
                        hbpdCount += 1;
                    }

                    hbCount += 1;
                    track  = true;
                } else {
                    /*var fbObject = {
                        "startedDateTime": startedDateTime,
                        "host": url.host,
                        "filename": items[i]
                    };*/
                    otherCount += 1;
                }
                if(track){
                testing = fbObject.host.replace(/\./g,"_");
                console.log('/directory/' + origfilepathname+"/"+testing);
                firebase.post('/directory/' + origfilepathname+"/"+testing, fbObject); //.then(console.log("Config Posted"));
                }
                //sumCount.analytics.push({"sa_queryString_Count": saqsCount});
                //sumCount.sa_postdata_Count = sapdCount;
                sumCount = {
                    "analytics": {
                        "QueryString": saqsCount,
                        "PostData": sapdCount,
                        "Total": saCount
                    },
                    "heartbeat": {
                        "Total": hbCount,
                        "QueryString": hbqsCount,
                        "PostData": hbpdCount
                    },
                    "other": otherCount
                };

                sum_saqsCount += saqsCount;
                sum_sapdCount += sapdCount;
                sum_saCount += saCount;
                sum_hbCount += hbCount;
                sum_hbqsCount += hbqsCount;
                sum_hbpdCount += hbpdCount;
                sum_otherCount += otherCount;
            };
            if(track){
            jsonClient.put('/directory/' + origfilepathname + '/config', sumCount); //.then(console.log("Config Posted"));
            }


        }
    };
    

    newCount = {
    "analytics": {
        "QueryString": sum_saqsCount,
        "PostData": sum_sapdCount,
        "Total": sum_saCount
    },
    "heartbeat": {
        "Total": sum_hbCount,
        "QueryString": sum_hbqsCount,
        "PostData": sum_hbpdCount
    },
    "other": sum_otherCount
};


jsonClient.put('/Count', newCount); //.then(console.log("Config Posted"));
}));

