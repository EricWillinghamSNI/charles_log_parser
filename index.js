var firebase = require('firebase');
var fs = require('fs');
var JSONPath = require('advanced-json-path');
var fileExtension = require('file-extension');


firebase.initializeApp({
    databaseURL: "https://sni-heartbeast-posts.firebaseio.com"
});

var basePath = "./Logs/UnProcessed/";

var db = firebase.database();

fs.readdir(basePath, function (err, items) {

    for (var i = 0; i < items.length; i++) {

        var origfile = basePath + items[i];

        db.ref('entries').remove();

        var origfilename = origfile.replace(/ /g, "_").split(".")[1];

        if (fileExtension(items[i]) == 'json') {

            // Get JSON File to be processed
            var object = require(origfile);
            var mid = JSONPath(object, "$..queryString[0][?(@.name == 's:user:mid')].value");

            // Get Array of Entries
            var entries = JSONPath(object, "$.log.entries");
                db.ref(mid).remove();

            for (var j = 0; j < entries.length; j++) {

                var entry = entries[j];
                // create Firebase entry
                var pushkey = db.ref('entries').push(entry);

                //console.log(entry);

                var queryStrings = JSONPath(entry, "$..queryString");
                var startedDateTime = JSONPath(entry, "$.startedDateTime");
                var sessionEntry = db.ref(mid).push(queryStrings);
                console.log(queryStrings);
                //.queryString[{$.Name=='s:user:mid'}]`");



            }
        }
    }

})